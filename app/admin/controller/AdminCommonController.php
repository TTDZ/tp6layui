<?php

namespace app\admin\controller;

use app\admin\model\SysmenuModel;
use app\admin\model\system\SyslogModel;
use app\admin\model\system\SysuserModel;
use app\common\controller\CommonController;
use think\facade\Config;
use think\facade\View;

class AdminCommonController extends CommonController
{
    
    protected function initialize()
    {
        parent::initialize();
        $path = $this->moduleName . '/' . $this->controllerName . '/' . $this->actionName;
        // var_dump($path);
        
        $dir="app\\".$this->moduleName."\\controller\\".str_replace(".","\\",$this->controllerName)."Controller";
        $data = get_class_methods($dir);
        $controllerMethods=[];
        foreach ($data as $index => $item) {
            $controllerMethods[$item]=$this->moduleName.'/'.$this->controllerName.'/'.$item;
        }
        
        $this->assign('controllerMethods',$controllerMethods);
        // var_dump($dir);
        // var_dump(session('menunames'));
        // var_dump($controllerMethods['add']);
        // var_dump(in_array($controllerMethods['add'],session('menunames')));
    }
}