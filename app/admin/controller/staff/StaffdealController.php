<?php

namespace app\admin\controller\staff;

use app\admin\controller\AuthController;
use app\admin\model\staff\StaffDealModel;

/**
 * Class IndexController
 * @package app\admin\controller
 */
class StaffdealController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            $staff_id = input('staff_id');
            $this->assign('staff_id', $staff_id);
            return $this->fetch();
        } else {
            $model = new StaffDealModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list->toArray()['data'];
        }
    }

    public function add()
    {
        if (!$this->isPost) {
            // $this->assign('jingjiaqudaolist', SysjingjiaqudaoService::getAllList());
            return $this->fetch();
        } else {
            $params = input('post.');
            $params['attachment']=json_encode($params['attachment']);
            $model = StaffDealModel::yqCreate($params);
            $this->success("成功！", "", $model);
        }
    }

    public function edit()
    {
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $params = input('post.');
            $params['attachment']=json_encode($params['attachment']);
            $model = StaffDealModel::yqUpdate($params);
            $this->success("成功！", "", $model);
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = StaffDealModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
