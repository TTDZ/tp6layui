<?php
namespace app\admin\controller\staff;
use app\admin\controller\AuthController;
use app\admin\model\staff\StaffModel;
use app\admin\model\system\SysuserModel;
use app\admin\validate\staff\StaffValidate;
use app\common\service\system\SyscasetypeService;
use app\common\service\system\SysdepartmentService;
use app\common\service\system\SysjingjiaqudaoService;
use app\common\service\system\SysmediaService;
use app\common\service\system\SysmediatypeService;
use app\common\service\system\SysuserService;
use think\Db;

class StaffController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            $mediatype_id = input('mediatype_id');
            $type = input('type');
            $chakan = input('chakan');
            $mediaflag = input('mediaflag');
            $this->assign('mediatype_id', $mediatype_id);
            $this->assign('type', $type);
            $this->assign('chakan', $chakan);
            $this->assign('nanfu', input('nanfu'));
            $this->assign('mediaflag', $mediaflag);
            /////////////////////////////////////////
            $this->assign('mediatypelist', SysmediatypeService::getListInProjectAndDepartment());
            $this->assign('doctorlist', "");
            $this->assign('stafftypelist', SyscasetypeService::getListInCase());
            $this->assign('medialist', SysmediaService::getListInProjectAndDepartment());
            // $Jingjiaqudao = A('Jingjiaqudao');
            // $Jingjiaqudaolist = $Jingjiaqudao->viewWidgetPatientList();
            $this->assign('jingjiaqudaolist', SysjingjiaqudaoService::getAllList());
            ///////////////////////////////////////////
            return $this->fetch();
        } else {
            $model = new StaffModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $params = input('post.');
            unset($params['id']);
            $validate = new StaffValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
            }
            try {
                $res = StaffModel::yqCreate($params);
                // var_dump($res->toArray());
                // exit();
                // $caseremark = [];
                // $caseremark['type'] = "咨询";
                // $caseremark['content'] = $params['addremark'];
                // $res->remarks()->save($caseremark);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if ($this->isPost) {
            $params = input('post.');
            $validate = new StaffValidate();
            if (!$validate->scene('edit')->check($params)) {
                $this->error($validate->getError());
            }
            $params['age'] = getIDCardInfo($params['card'])['age'];
            $params['birthday'] = getIDCardInfo($params['card'])['birthday'];
            $params['sex'] = getIDCardInfo($params['card'])['sex'];
            $model = StaffModel::yqUpdate($params);
            // if (!empty(trim($params['addremark']))) {
            //     $caseremark = [];
            //     $caseremark['type'] = "咨询";
            //     $caseremark['content'] = $params['addremark'];
            //     $model->remarks()->save($caseremark);
            // }
            $this->success("更新成功！", "", $params);
        } else {
            $this->assign('departmentlist', SysdepartmentService::getAllList());
            return $this->fetch();
        }
    }

    public function goudao()
    {
        if (!$this->isPost) {
            $this->assign('usertananlist', SysuserService::getUsertananListInCase());
            return $this->fetch();
        } else {
            $params = input('post.');
            $model = StaffModel::yqGetOne($params['id']);
            $params['yuyuetime'] = date('Y-m-d H:i:s');
            $params['statusdao'] = '已到';
            if ($model->statusdao == "已到") {
                unset($params['yuyuetime']);
            } else {
                $params['usertanan_name'] = SysuserModel::yqGetField([['id', '=', $params['usertanan_id']]], 'account');
            }
            $model = StaffModel::yqUpdate($params);
            if (!empty(trim($params['addremark']))) {
                $caseremark = [];
                $caseremark['type'] = "前台";
                $caseremark['content'] = $params['addremark'];
                $model->remarks()->save($caseremark);
            }
            $this->success("勾到成功！", "", $params);
        }
    }

    public function chakan()
    {
        if (!$this->isPost) {
            $this->assign('departmentlist', SysdepartmentService::getAllList());
            return $this->fetch();
        } else {
        }
    }

    public function huifang()
    {
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $params = input('post.');
            $model = StaffModel::yqGetOne($params['id']);
            if ($model->statusdao == "已到") {
                $this->success("已到不能回访，请刷新界面！", "", $params);
            } else {
                $model->yuyuetime = $params['yuyuetime'];
                $model->statusdao = '回访';
                $model->save();
                $data['type'] = $params['type'];
                $data['content'] = $params['content'];
                $data['remark'] = $params['remark'];
                $data['next_time'] = $params['next_time'];
                $model->visits()->save($data);
                $this->success("成功！", "", $params);
            }
        }
    }

    public function weihuifang()
    {
    }

    public function duanxin()
    {
    }

    public function zizhi()
    {
        if (!$this->isPost) {
            // $this->assign('jingjiaqudaolist', SysjingjiaqudaoService::getAllList());
            return $this->fetch();
        } else {
            $params = input('post.');
            $model = StaffModel::yqGetOne($params['id']);
            unset($params['id']);
            $model->talks()->save($params);
            $this->success("成功！", "", $model);
        }
    }

    public function hetong()
    {
    }

    public function chakanhuifang()
    {
    }

    public function yidong()
    {
    }

    public function peixun()
    {
    }

    public function jingjiaqudao()
    {
        if (!$this->isPost) {
            $this->assign('jingjiaqudaolist', SysjingjiaqudaoService::getAllList());
            return $this->fetch();
        } else {
            $params = input('post.');
            $model = StaffModel::yqUpdate($params);
            $this->success("成功！", "", $model);
        }
    }

    public function huishou()
    {
    }

    public function daochuexcel()
    {
    }

    public function delete()
    {
        $ids = input('ids');
        $mdeol = StaffModel::yqGetOne($ids);
        $mdeol->isdel = 0;
        $res = $mdeol->save();
        $this->success("成功！", "", $res);
    }
}
