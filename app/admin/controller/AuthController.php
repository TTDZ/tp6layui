<?php

namespace app\admin\controller;

use app\admin\model\system\SysuserModel;
use auth\Auth;

class AuthController extends AdminCommonController
{
    protected function initialize()
    {
        parent::initialize();
        $path = $this->moduleName . '/' . $this->controllerName . '/' . $this->actionName;
        $auth = new Auth();
        $session_admin_account = session('sysuser.account');
        if (empty($session_admin_account)) {
            if ($this->isPost) {
                // header("Location:" . url("admin/public/index"));
                // 				$this->error("您还没有登录！", url("admin/public/login"));
            } else {
                // header("Location:" . url("admin/public/index"));
                $this->redirect(url("admin/public/index"));
            }
        } else {
            $this->showUserStatus();
            if ($session_admin_account != "admin") {//如果不是Administrator，根据权限组ID进行验证
                if (!$auth->check($path, session('sysuser.id'))) {
                    if ($this->isAjax) {
                        $this->error("您没有访问权限！");
                    } else {
                        // $this->error("您没有访问权限！");
                        echo("您没有访问权限！");
                    }
                    exit();
                }
            }
        }
    }

    public function showUserStatus()
    {
        $sysuser = SysuserModel::yqGetOne(session('sysuser.id'));
        if ($sysuser->status == '禁用') {
            session(null);
            // header("Location:" . url("admin/public/index"));
            // $this->error("您的账户已被禁用！请联系管理员解除！",url("admin/public/index"));
            $this->redirect(url("admin/public/index"));
            exit;
        }
    }
}