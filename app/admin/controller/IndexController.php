<?php

namespace app\admin\controller;


use app\common\service\system\SysprojectService;

/** 杨庆
 * Class IndexController
 * @package app\admin\controller
 */
class IndexController extends AuthController
{
    /**
     * 杨庆
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        // halt(session());
        // halt(erwei(session('sysuser.sysmenu')));
        foreach (session('sysuser.sysrole') as $item) {
            $roles[] = $item['title'];
        }
        $this->assign('sysuser_role', implode('|', $roles));
        $this->assign("menulist", erwei(session('sysuser.sysmenu')));
        $projectlist = SysprojectService::getUserProjectList();
        session('projectid', $projectlist->toArray()[0]['id']);
        session('projectname', $projectlist->toArray()[0]['name']);
        $this->assign("projectlist", json_encode($projectlist, JSON_UNESCAPED_UNICODE));
        
        return $this->fetch();
    }

    public function upload()
    {
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('image');
        dump($file);
        // 上传到本地服务器
        $savename = \think\facade\Filesystem::putFile('topic', $file);
        dump($savename);
    }
}
