<?php

namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use app\admin\controller\WidgetController;
use app\admin\model\system\SysdepartmentModel;
use app\admin\model\system\SysmediaModel;
use app\admin\validate\system\SysprojectValidate;
use app\common\service\system\SysdepartmentService;
use app\common\service\system\SysmediaService;
use think\Db;

class SysdepartmentController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $model = new SysdepartmentModel();
            $list = $model->getIndexData();
            // return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list->toArray()['data'];
            return $list;
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $params = input('post.');
            unset($params['id']);
            $mediaids = input('post.mediaids');
            if (empty($mediaids)) {
                $this->error("请选择渠道来源！", "", $params);
                exit;
            }
            $validate = new SysprojectValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            try {
                $res = SysdepartmentModel::yqCreate($params);
                $model = SysdepartmentModel::yqGetOne($res->id);
                $model->medias()->detach();//删除关联中间表数据
                $model->medias()->saveAll($mediaids);
                $map[] = ['id', 'in', $mediaids];
                $tempids = SysmediaModel::yqGetColumn($map, 'mediatype_id');
                $model->mediatypes()->detach();//删除关联中间表数据
                $model->mediatypes()->saveAll(array_unique($tempids));
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if ($this->isPost) {
            $params = input('post.');
            $mediaids = input('post.mediaids');
            if (empty($mediaids)) {
                $this->error("请选择渠道来源！", "", $params);
                exit;
            }
            SysdepartmentModel::update($params);
            $model = SysdepartmentModel::yqGetOne($params['id']);
            $model->medias()->detach();//删除关联中间表数据
            $model->medias()->saveAll($mediaids);
            $map[] = ['id', 'in', $mediaids];
            $tempmediatype_ids = SysmediaModel::yqGetColumn($map, 'mediatype_id');
            $model->mediatypes()->detach();//删除关联中间表数据
            $model->mediatypes()->saveAll(array_unique($tempmediatype_ids));
            $this->success("成功！", "", $params);
        } else {
            $this->assign('departmentlist', SysdepartmentService::getAllList());
            $this->assign('medialist', SysmediaService::getAllList());
            return $this->fetch();
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = SysdepartmentModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
