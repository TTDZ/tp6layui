<?php

namespace app\admin\model\staff;

use app\admin\model\system\SyscasetypeModel;
use app\admin\model\system\SysdepartmentModel;
use app\admin\model\system\SysjingjiaqudaoModel;
use app\admin\model\system\SysmediaModel;
use app\admin\model\system\SysmediatypeModel;
use app\admin\model\system\SysprojectModel;
use app\admin\model\system\SysuserModel;
use app\common\model\CommonModel;
use app\common\service\system\SysdepartmentService;
use app\common\service\system\SysmediatypeService;

class StaffModel extends CommonModel
{
    protected $name = "staff";
    protected $autoWriteTimestamp = 'datetime';

    //////////////////////////////////////////////////////////////
    public function user()
    {
        return $this->belongsTo(SysuserModel::class, 'user_id');
    }

    public function project()
    {
        return $this->belongsTo(SysprojectModel::class, 'project_id');
    }

    public function changes()
    {
        return $this->hasMany(StaffChangesModel::class, 'staff_id');
    }

    public function zizhis()
    {
        return $this->hasMany(StaffZizhiModel::class, 'staff_id');
    }

    public function deals()
    {
        return $this->hasMany(StaffDealModel::class, 'staff_id');
    }

    public function peixuns()
    {
        return $this->hasMany(StaffPeixunModel::class, 'staff_id');
    }

    //////////////////////////////////////////////////////////////
    public function setMediaIdAttr($value, $data)
    {
        return $value;
    }

    public function setProjectIdAttr($value, $data)
    {
        return $value;
    }

    //////////////////////////////////////////////////////////////
    public static function onBeforeInsert($model)
    {
        $model->project_id = session('projectid');
        $model->project_name = session('projectname');
        $model->user_id = session('sysuser.id');
        $model->user_name = session('sysuser.account');
    }

    public static function onBeforeUpdate($model)
    {
        $model->casetype_name = SyscasetypeModel::yqGetField([['id', '=', $model->casetype_id]], 'name');
        $model->mediatype_id = SysmediaModel::yqGetField([['id', '=', $model->media_id]], 'mediatype_id');
        $model->mediatype_name = SysmediatypeModel::yqGetField([['id', '=', $model->mediatype_id]], 'name');
        $model->media_name = SysmediaModel::yqGetField([['id', '=', $model->media_id]], 'name');
        $model->project_name = session('projectname');
        $model->department_name = SysdepartmentModel::yqGetField([['id', '=', $model->department_id]], 'name');
    }

    //////////////////////////////////////////////////////////////
    public function getIndexData()
    {
        $page = input('page');
        $pageSize = input('pageSize');
        $sort = input('sort');
        $order = input('order');
        $where = array();
        $where[] = ['isdel', '=', 1];
        $where[] = ['project_id', '=', session('projectid')];
        ///////////////////////////////普查询参数
        $num = input('num');
        if (!empty($num)) {
            $where[] = ['num', '=', $num];
        }
        $name = input('name');
        if (!empty($name)) {
            $where[] = ['name', 'like', '%' . $name . '%'];
        }
        $lianxiren = input('lianxiren');
        if (!empty($lianxiren)) {
            $where[] = ['lianxiren', 'like', '%' . $lianxiren . '%'];
        }
        $tel = input('tel');
        if (!empty($tel)) {
            $where[] = ['tel', 'like', $tel . '%'];
        }
        $yixiang = input('yixiang');
        if (!empty($yixiang)) {
            $where[] = ['yixiang', '=', $yixiang];
        }
        $fenlei = input('fenlei');
        if (!empty($fenlei)) {
            $where[] = ['fenlei', '=', $fenlei];
        }
        $guishu = input('guishu');
        if (!empty($guishu)) {
            $where[] = ['guishu', '=', $guishu];
        }
        $status = input('status');
        if (!empty($status)) {
            if ($status == '未到') {
                $where[] = ['status', '<>', "已到"];
            } else {
                $where[] = ['status', '=', $status];
            }
        }
        $position_level = input('position_level');
        if (!empty($position_level)) {
            $where[] = ['position_level', '=', $position_level];
        }
        $mediatype_id = input('mediatype_id');
        if (!empty($mediatype_id)) {
            $where[] = ['mediatype_id', '=', $mediatype_id];
        }
        $media_id = input('media_id');
        if (!empty($media_id)) {
            $where[] = ['media_id', '=', $media_id];
        }
        $casetype_id = input('casetype_id');
        if (!empty($casetype_id)) {
            $where[] = ['casetype_id', '=', $casetype_id];
        }
        $ruzhitimestart = input('ruzhitimestart');
        $ruzhitimeend = input('ruzhitimeend');
        $create_timestart = input('create_timestart');
        $create_timeend = input('create_timeend');
        if (!empty($ruzhitimestart) && !empty($ruzhitimeend)) {
            $where[] = ['ruzhi_time', 'between', array($ruzhitimestart . ' 00:00:00', $ruzhitimeend . ' 23:59:59')];
        }
        if (!empty($create_timestart) && !empty($create_timeend)) {
            $where[] = ['create_time', 'between', array($create_timestart . ' 00:00:00', $create_timeend . ' 23:59:59')];
        }
        ///////////////////////////////高级查询参数
        // $mediatype_id = input('mediatype_id', '');
        // $media_id = input('media_id', '');
        // $type = input('type');
        // $chakan = input('chakan');
        // $mediaflag = input('mediaflag');
        // if ($type != null && $type != "") {
        //     if ($mediaflag == 'mediatype') {
        //         if ($mediatype_id != 0) {
        //             $where[] = ['mediatype_id', '=', $mediatype_id];
        //         } else {
        //             //如果获取总数据则需要获取部门和机构的交集
        //             $mediatypeids_project = SysmediatypeService::getIdsInProject(session('projectid'));
        //             $mediatypeids_department = SysmediatypeService::getIdsInDepartment(session('sysuser.department_id'));
        //             $meidatypeid = array_intersect($mediatypeids_project, $mediatypeids_department);
        //             $where[] = ['mediatype_id', 'in', $meidatypeid];
        //         }
        //     } else {
        //         $where[] = ['media_id', '=', $mediatype_id];
        //     }
        //     $where = $this->getConditionByType($where, $type);
        // } else {
        //     if ($mediatype_id != null && $mediatype_id != "") {
        //         $where[] = ['mediatype_id', '=', $mediatype_id];
        //     }
        //     if ($media_id != null && $media_id != "") {
        //         $where[] = ['media_id', '=', $media_id];
        //     }
        // }
        // ///////////////////////////////高级查询参数
        /////////////////////////////////////////////模糊查询优先级最高
        $mohu = input("mohu", '', 'trim');
        if ($mohu != null && $mohu != "") {
            $where[] = ['name|lianxiren|tel|zixuncontent|user_name|area', 'like', '%' . $mohu . '%'];
        }
        /////////////////////////////////////////////模糊查询优先级最高
        $model = $this->where($where)
                      ->with([
                          "user", "project", 'changes',
                          'zizhis' => function ($query) {
                              $query->order('create_time', 'desc');
                          },
                          "deals"  => function ($query) {
                              $query->order('create_time', 'desc');
                          },
                          "peixuns"  => function ($query) {
                              $query->order('create_time', 'desc');
                          },
                      ])
                      ->order($sort, $order)->paginate($pageSize);
        // var_dump($this->getLastSql());
        return $model;
    }
}