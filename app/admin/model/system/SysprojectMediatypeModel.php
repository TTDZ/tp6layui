<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysprojectMediatypeModel extends CommonModel
{
    protected $name = "sysproject_mediatype";
    protected $autoWriteTimestamp = 'datetime';
}