<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysdepartmentMediaModel extends CommonModel
{
    protected $name = "sysdepartment_media";
    protected $autoWriteTimestamp = 'datetime';
}