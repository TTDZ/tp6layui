<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysdepartmentMediatypeModel extends CommonModel
{
    protected $name = "sysdepartment_mediatype";
    protected $autoWriteTimestamp = 'datetime';
}