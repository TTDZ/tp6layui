<?php

namespace app\admin\model\system;

use app\admin\model\system\SysprojectModel;
use app\admin\model\system\SysuserModel;
use app\common\model\CommonModel;

class SysmenuModel extends CommonModel
{
    protected $name = "sysmenu";

    public function user()
    {
        return $this->belongsTo(SysuserModel::class, 'userid');
    }

    public function project()
    {
        return $this->belongsTo(SysprojectModel::class, 'projectid');
    }

    public function hospital()
    {
        return $this->belongsTo(HospitalModel::class, 'hospitalid');
    }

    public function patientinforms()
    {
        return $this->hasMany(SysroleModel::class, 'patientid');
    }

    public static function getAllMenus()
    {
        $menus=self::select();
        return $menus;
    }


}