<?php

namespace app\admin\validate\staff;

use think\Validate;

class StaffValidate extends Validate
{
    protected $rule = [
        'name' => 'require|max:255',
        'tel'  => 'require|mobile',
        'card' => 'require|idCard|max:20|unique:staff',
    ];
    protected $message = [
        'name.require' => '名称必须！',
        'name.max'     => '名称最多不能超过255个字符！',
        'tel.require'  => '联系电话必须！',
        'tel.mobile'   => '联系电话格式不正确！',
        'card.require' => '身份证号码必须！',
        'card.idCard' => '身份证号码无效',
        'card.max'     => '身份证号码最多不能超过20个字符！',
        'card.unique'  => '身份证号码已经存在！',
    ];
    protected $scene = [
        'edit' => ['name', 'tel'],
    ];
}