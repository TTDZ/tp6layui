<?php

namespace app\common\service\system;

use app\admin\model\system\SysprojectModel;
use think\Service;

class SysprojectService extends Service
{
    public static function getAllList()
    {
        $model = new SysprojectModel();
        return $model->where([])->order('id', 'asc')->select();
        
    }
    public static function getUserProjectList()
    {
        $model = new SysprojectModel();
        $res = $model->alias('a')->field('a.id,a.name')
                     ->join(['sysproject_user' => 'pu'], 'a.id=pu.project_id')
                     ->where('pu.user_id', '=', session('sysuser.id'))
                     ->select();
        return $res;
    }
}