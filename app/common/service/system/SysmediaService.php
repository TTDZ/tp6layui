<?php

namespace app\common\service\system;

use app\admin\model\system\SysdepartmentMediaModel;
use app\admin\model\system\SysmediaModel;
use app\admin\model\system\SysprojectMediaModel;
use think\Service;

class SysmediaService extends Service
{
    public static function getAllList()
    {
        $model = new SysmediaModel();
        return $model->with(['mediatype'])->where("status", '可用')->order('mediatype_id', 'asc')->select();
    }

    public static function getListInProjectAndDepartment()
    {
        $mediaids_department = self::getIdsInDepartment(session('sysuser.department_id'));
        $mediaids_project = self::getIdsInProject(session('projectid'));
        $mediaids = array_intersect($mediaids_department, $mediaids_project);
        $res = (new SysmediaModel())->where([['id', 'in', $mediaids]])->with(['mediatype'])->order("mediatype_id", "asc")->select();
        // var_dump($res->toArray());
        return $res;
    }

    public static function getIdsInProject($project_id)
    {
        return SysprojectMediaModel::yqGetColumn([['project_id', '=', $project_id]], 'media_id');
    }

    public static function getIdsInDepartment($department_id)
    {
        return SysdepartmentMediaModel::yqGetColumn([['department_id', '=', $department_id]], 'media_id');
    }
}