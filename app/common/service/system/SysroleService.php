<?php

namespace app\common\service\system;

use app\admin\model\system\SysmenuModel;
use app\admin\model\system\SysprojectModel;
use app\admin\model\system\SysroleModel;
use think\Service;

class SysroleService extends Service
{
    public static function getAllList()
    {
        $sysrolemodel = new SysroleModel();
        return $sysrolemodel->where("status", 1)->select();
        
    }
}