<?php

namespace app\common\service\system;

use app\admin\model\system\SysdepartmentModel;
use think\facade\Db;
use think\Service;

class SysdepartmentService extends Service
{
    public static function getAllList()
    {
        $model = new SysdepartmentModel();
        $where[]=['status','=','可用'];
        $where[]=['project_id','=',session('projectid')];
        return $model->where($where)->order('id', 'asc')->select();
    }

    public static function getDepartmentMediaIds($department_id)
    {
        return Db::name('sysdepartment_mediatype')->where([['department_id','=',$department_id]])->column('mediatype_id');
        // return SysmediatypeModel::yqGetColumn([['department_id','=',$department_id]],'mediatype_id');
    }

    public static function getDepartmentMediaTypeIds($department_id)
    {
        return Db::name('sysdepartment_media')->where([['department_id','=',$department_id]])->column('media_id');
        // return SysmediaModel::yqGetColumn([['department_id','=',$department_id]],'media_id');
    }
}