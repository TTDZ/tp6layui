//导航的hover效果、二级菜单等功能，需要依赖element模块
$('.larry-side-menu').click(function () {
    if (trun) {
        $('.x-side').animate({left: '0px'}, 200).siblings('.x-main').animate({left: '200px'}, 200);
        trun = 0;
    } else {
        $('.x-side').animate({left: '-200px'}, 200).siblings('.x-main').animate({left: '0px'}, 200);
        trun = 1;
    }
});
$('body').on('dblclick', '.layui-tab-title li', function (elem) {
    var id = $(this).attr("lay-id");
    if (id == "0") {
        layer.msg('首页不能关闭！');
    } else {
        layui.element.tabDelete("x-tab", $(this).attr("lay-id"));//删除
    }
});
/**
 * 添加tab
 */
function addTab(id, title, url, icon, iframe = false) {
    var li = $(".layui-tab-title li[lay-id=" + id + "]").length;
    //console.log(li);
    if (li > 0) {
        //tab已经存在直接切换tab
        layui.element.tabChange('x-tab', id);
    } else {
        //var indexLoading = layer.load(0, { shade: false }); //0代表加载的风格，支持0-2
        var indexLoading = layer.msg('页面加载中.请稍后...');
        $.get(url, {}, function (str) {
            //创建tab
            // console.log(str);
            if (iframe) {
                layui.element.tabAdd('x-tab', {
                    id: id,
                    title: '<i class="layui-icon ' + icon + ' layui-anim layui-anim-rotate"></i>&nbsp;' + title + '', //用于演示
                    autoRefresh: true,
                    contextMenu: true,
                    content: '<iframe frameborder="0" src="' + url + '" class="x-iframe"></iframe>'
                });
            } else {
                layui.element.tabAdd('x-tab', {
                    id: id,
                    title: '<i class="layui-icon ' + icon + ' layui-anim layui-anim-rotate"></i>&nbsp;' + title + '', //用于演示
                    autoRefresh: true,
                    contextMenu: true,
                    // content: '<iframe frameborder="0" src="' + url + '" class="x-iframe"></iframe>'
                    content: "<span id='externalHtml" + id + "'></span>"
                });
                $("#externalHtml" + id).html(str);
            }
            layui.element.tabChange('x-tab', id);
            layer.close(indexLoading);
        }).error(function () {
            layer.close(indexLoading);
            layer.msg('页面加载出错.请确认是否存在...');
        });
    }
}
/*弹出层*/
/*
	参数解释：
	title	标题
	url		请求的url
	id		需要操作的数据id
	w		弹出层宽度（缺省调默认值）
	h		弹出层高度（缺省调默认值）
*/
function x_admin_show(title, url, w, h) {
    if (title == null || title == '') {
        title = false;
    }
    ;
    if (url == null || url == '') {
        url = "404.html";
    }
    ;
    if (w == null || w == '') {
        w = 800;
    }
    ;
    if (h == null || h == '') {
        h = ($(window).height() - 50);
    }
    ;
    layer.open({
        type: 2,
        offset: 't',
        area: [w + 'px', h + 'px'],
        fix: false, //不固定
        maxmin: true,
        shadeClose: true,
        shade: 0.4,
        scrollbar: false,
        zIndex: 999,
        title: title,
        content: url,
        btn: ['按钮一', '按钮二', '按钮三'],
        yes: function (index, layero) {
            //按钮【按钮一】的回调
            console.log(index);
            console.log(layero);
        },
        btn2: function (index, layero) {
            //按钮【按钮二】的回调
            //return false 开启该代码可禁止点击该按钮关闭
        },
        btn3: function (index, layero) {
            //按钮【按钮三】的回调
            //return false 开启该代码可禁止点击该按钮关闭
        },
        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
        }
    });
}
/*关闭弹出框口*/
function x_admin_close() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}
function fillForm(obj, o) {
    var enhance = new enhanceForm({
        elem: obj
    });
    enhance.filling(o);
}
function fullscreen() {
    var ac = 'layui-icon-screen-full', ic = 'layui-icon-screen-restore';
    var ti = $(this).find('i');
    var isFullscreen = document.fullscreenElement || document.msFullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || false;
    if (isFullscreen) {
        var efs = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen;
        if (efs) {
            efs.call(document);
        } else if (window.ActiveXObject) {
            var ws = new ActiveXObject('WScript.Shell');
            ws && ws.SendKeys('{F11}');
        }
        ti.addClass(ac).removeClass(ic);
    } else {
        var el = document.documentElement;
        var rfs = el.requestFullscreen || el.webkitRequestFullscreen || el.mozRequestFullScreen || el.msRequestFullscreen;
        if (rfs) {
            rfs.call(el);
        } else if (window.ActiveXObject) {
            var ws = new ActiveXObject('WScript.Shell');
            ws && ws.SendKeys('{F11}');
        }
        ti.addClass(ic).removeClass(ac);
    }
}
function popupRight(path, title) {
    var index = layer.open({
        area: ['350px', '100%'],
        shadeClose: true,
        title: title,
        //path: path,
        type: 2,
        offset: 'r',
        //shade: 2,
        skin: 'layui-layer-adminRight',
        anim: 5,//0平滑放大默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        isOutAnim: true,
        closeBtn: false,
        resize: false,
        //btn: ['提交保存', '取消添加'],
        content: path,
        yes: function (index, layero) {
            var iframeWin = window[layero.find('iframe')[0]['name']];
            if (iframeWin.checkform()) {
            }
        },
        success: function (layero, index) {
            //$(layero).children('.layui-layer-content').load(path);
            //layer.iframeAuto(index);
        },
        end: function () {
            layer.closeAll('tips');
        },
        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
        }
    })
}
function yq_dtree(dTree_pid, option, callback) {
    var dTree_pid = layui.dtree.renderSelect({
        elem: "#" + dTree_pid,
        width: "100%", // 可以在这里指定树的宽度来填满div
        url: option.url,
        none: "没有数据？",
        selectTips: "请选择",
        dataStyle: "layuiStyle",  //使用layui风格的数据格式
        dataFormat: "list",  //配置data的风格为list
        // icon: "2",  //修改二级图标样式
        initLevel: "5",  //默认展开层级为1
        selectCardHeight: "300",
        skin: "layui",
        response: {
            statusName: "code", //返回标识（必填）
            statusCode: 1, //返回码（必填）
            message: "msg", //返回信息（必填）
            rootName: "data", //根节点名称（必填）
            treeId: "id", //节点ID（必填）
            parentId: "pid", //父节点ID（必填）
            title: "title", //节点名称（必填）
            // ficonClass: "ficonClass", //自定义一级图标class（v2.5.4版本新增）（非必填）
            // iconClass: "iconClass", //自定义二级图标class（非必填）
            childName: "children", //子节点名称（默认数据格式必填）
            last: "last", //是否最后一级节点（true：是，false：否，布尔值，非必填）
            level: "level", //层级（v2.4.5_finally_beta版本之后，该属性移除）
            spread: "spread", //节点展开状态（v2.4.5_finally_beta版本新增。true：展开，false：不展开，布尔值，非必填）
            disabled: "disabled", //节点禁用状态（v2.5.0版本新增。true：禁用，false：不禁用，布尔值，非必填）
            hide: "hide", //节点隐藏状态（v2.5.0版本新增。true：隐藏，false：不隐藏，布尔值，非必填）
            checkArr: "checkarr", //复选框列表（开启复选框必填，默认是json数组。）
            checked: "checked", //是否选中（开启复选框，0-未选中，1-选中，2-半选。非必填）
            type: "type", //复选框标记（开启复选框，从0开始。非必填）
            basicData: "basicData" //表示用户自定义需要存储在树节点中的数据（非必填）
        }, // 这里指定了返回的数据格式，组件会根据这些值来替换返回JSON中的指定格式，从而读取信息
        checkbar: option.checkbar,//要开启复选框数据源里面必须有checkarr 字段   并且不能为null
        checkbarType: "self", // 默认就是all，其他的值为： no-all  p-casc   self  only
        formatter: {
            title: function (data) {  // 示例给有子集的节点返回节点统计
                var s = data.title + "(" + data.name + ")";
                if (data.children) {
                    s += ' <span style="color:blue">(' + data.children.length + ')</span>';
                }
                return s;
            },
            checkArr: function (data) {  // 示例给有子集的节点返回节点统计
                var s = data.name;
                return "0";
            },
        },
        done: function (res, $ul, first) {
            if (first) {
                callback();
            }
            // var userMenuList = '';
            // userMenuList = JSON.parse(userMenuList);
            // var ids = [];
            // $.each(userMenuList, function (index, item) {
            //     console.log(item);
            //     ids.push(item.id);
            // });
            // //dtree.dataInit("dTree_nodeids",ids.join(','));
            // var params = dtree.chooseDataInit("dTree_nodeids", ids.join(','));
            // //dtree.setSelectValue("dTree_nodeids"); // 也可以在这里指定，第二个参数如果不填，则会自动读取
        }
    });
}