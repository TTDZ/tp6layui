# tp6layui

#### 介绍
在之前项目的基础上更新到thinkphp6+layui2.6.8，可以基于本系统开发任何企业级系统包括ERP，CRM，HIS等等，现在开源给大家希望大家喜欢，另外本人另一个基于springboot+layui的小型权限管理系统已经开源了，地址是：[springboot+layui的小型权限管理系统](https://gitee.com/yq5858588/demospringboot)  也希望大家喜欢

#### 软件架构
tp6.0.8+layui2.6.8+author权限

###更新说明

1. 大量修改了layui的样式文件，之前layui的整体风格有点大，包括表格和表单元素，现在把整体风格调整的小了很多更加紧凑，这样更加适合开发业务场景比较复杂的系统，像医院的his系统，企业的erp系统和人事oa等。
2. thinkphp框架从5版本升级到了6.0.8最新版
3. 删除了以前很多不用了js文件，整个包小了很多，里面保留了ueditor   ckfinder   以及echarts


#### 安装教程
下载下来直接修改数据库文件的链接地址就可以了  ，记得把数据库先导入到自己的电脑中，项目根目录有数据库文件

#### 使用说明

默认最高权限的用户是admin，密码是123456
![这个是动态图片](https://images.gitee.com/uploads/images/2019/0829/094600_569e0dd1_383370.gif "GIF.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0828/172731_c88e3294_383370.png "TIM截图20190828171801.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0828/172750_fa211ca0_383370.png "TIM截图20190828171951.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0828/172758_f3c4a2ba_383370.png "TIM截图20190828172001.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0828/172808_2168e788_383370.png "TIM截图20190828172016.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0828/172815_b3e52b1f_383370.png "TIM截图20190828172036.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0828/172823_ec9620ff_383370.png "TIM截图20190828172050.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170838_bc522b6f_383370.jpeg "在这里输入图片标题")
喜欢的话给个打赏哦 :stuck_out_tongue_winking_eye: 

